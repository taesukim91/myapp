<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $tiemstamps = false;
    protected $fillable = ['title', 'body'];
}
